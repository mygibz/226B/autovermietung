﻿using System;
using System.Collections.Generic;
using Autovermietung.Exception;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Autovermietung.Test
{
    [TestClass]
    public class ContractUnitTest
    {
        private static Customer _customer = new Customer("Max Mustermann", Language.De);
        private static Employee _employee = new Employee("Maxi Musti", Language.De);
        private static List<Vehicle> _vehicles = new List<Vehicle>();

        [TestMethod]
        public void TestIsValid_Valid()
        {
            // arrange
            DateTime date = DateTime.Today;
            License license = new License(_customer);
            _customer.License = license;
            Employee employee = new Employee("Maxi Musti", Language.De);
            Contract contract = new Contract(_customer, _employee, new List<Vehicle>(), date, date);
            
            // act
            
            // assert
            Assert.IsTrue(contract.IsValid());
        }
        
        [TestMethod]
        public void TestIsValid_Invalid()
        {
            // arrange
            DateTime date = new DateTime(2020, 11, 10);
            Customer customer = new Customer("Max Mustermann", Language.De);
            License license = new License(customer);
            customer.License = license;
            Employee employee = new Employee("Maxi Musti", Language.De);
            Contract contract = new Contract(customer, employee, new List<Vehicle>(), date, date);
            
            // act
            
            // assert
            Assert.IsFalse(contract.IsValid());
        }
        
        [TestMethod]
        public void TestNoEqupment()
        {
            // arrange
            DateTime date = new DateTime(2020, 11, 10);
            Customer customer = new Customer("Max Mustermann", Language.De);
            License license = new License(customer);
            customer.License = license;
            Employee employee = new Employee("Maxi Musti", Language.De);
            Contract contract = new Contract(customer, employee, new List<Vehicle>(), date, date);
            
            // act
            
            // assert
            // Assert.AreEqual(contract.GetEquipment().Count, 0);
        }
        
        [TestMethod]
        public void TestEquipment()
        {
            // arrange
            DateTime date = new DateTime(2020, 11, 10);
            Customer customer = new Customer("Max Mustermann", Language.De);
            License license = new License(customer);
            customer.License = license;
            Employee employee = new Employee("Maxi Musti", Language.De);
            Equipment equipment = new Equipment("Equipment", "Some equipment");
            Contract contract = new Contract(customer, employee, new List<Vehicle>(), date, date);
            // contract.AddEquipment(equipment);
            
            // act

            // assert
            // Assert.AreEqual(contrackt.GetEquipment().Count, 1);
        }
        
        [TestMethod]
        public void TestInvalidLicense()
        {
            // arrange
            DateTime date = new DateTime(2020, 11, 10);
            Customer customer = new Customer("Max Mustermann", Language.De);
            License license = new License(customer, false);
            customer.License = license;
            Employee employee = new Employee("Maxi Musti", Language.De);
            
            // act => assert
            Assert.ThrowsException<InvalidLicenseException>(() =>
                new Contract(customer, employee, new List<Vehicle>(), date, date));
        }
        
        [TestMethod]
        public void TestNoLicense()
        {
            // arrange
            DateTime date = new DateTime(2020, 11, 10);
            Customer customer = new Customer("Max Mustermann", Language.De);
            Employee employee = new Employee("Maxi Musti", Language.De);
            
            // act

            // act => assert
            Assert.ThrowsException<InvalidLicenseException>(() =>
                new Contract(customer, employee, new List<Vehicle>(), date, date));
        }
    }
}