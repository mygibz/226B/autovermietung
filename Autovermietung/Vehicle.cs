using System;

namespace Autovermietung
{
    [Serializable]
    public abstract class Vehicle
    {
        public Guid Id { get; }
        public Manufacturer Manufacturer { get; }
        public string Model { get; }
        public int Doors { get; }
        public int XWheelDrive { get; }
        public bool IsSelfDriving { get; }
        public bool HasTrailerHitch { get; set; }
        public Plate Plate { get; set; }
        public float PricePerDay { get; set; }

        public bool CurrentlyInUse { get; set; }
        public bool CurrentlyDamaged { get; set; }
        public string CurrentlyAssignedMechanic { get; set; }

        public string UUID { get; set; }

        protected Vehicle(Manufacturer manufacturer, string model, float pricePerDay, int doors, int xWheelDrive,
            bool isSelfDriving = false, bool hasTrailerHitch = false, Plate plate = null)
        {
            Id = Guid.NewGuid();
            Manufacturer = manufacturer;
            Model = model;
            PricePerDay = pricePerDay;
            Doors = doors;
            XWheelDrive = xWheelDrive;
            IsSelfDriving = isSelfDriving;
            HasTrailerHitch = hasTrailerHitch;
            Plate = plate;

            CurrentlyAssignedMechanic = null;
            CurrentlyDamaged = false;
            CurrentlyInUse = false;
            UUID = Guid.NewGuid().ToString();
        }

        public virtual void PrintInfo()
        {
            Console.Write($"Vehicle: \t{Manufacturer.Name} {Model}\n" +
                          $"\t\t Driven axis:\t{XWheelDrive}\n" +
                          $"\t\t Price per day:\t{PricePerDay}\n" +
                          $"\t\t Doors: \t{Doors}\n" +
                          $"\t\t Plate: \t{(Plate == null ? "Not registered" : Plate.Number)}\n" +
                          $"\t\t Trailer hitch: {(HasTrailerHitch ? "Yes" : "No")}\n" +
                          $"\t\t Self driving: {(IsSelfDriving ? "Yes" : "No")}\n");
        }
    }
}