﻿using System;

namespace Autovermietung
{
    public class Car : Vehicle
    {
        public int Seats { get; }
        public Car(Manufacturer manufacturer, string model, float pricePerDay, int doors, int xWheelDrive, int seats = 4,
            bool isSelfDriving = false, bool hasTrailerHitch = false, Plate plate = null) :
            base(manufacturer, model, pricePerDay, doors, xWheelDrive, isSelfDriving, hasTrailerHitch, plate)
        {
            Seats = seats;
        }

        public override void PrintInfo()
        {
            base.PrintInfo();
            Console.Write($"\t\t Seats: \t{Seats}\n");
            // TODO: SAMUEEEEEEEEEEEELLLLLLLLL!!!!!!!!!!!!!!!! FINISH YOUR STUFF
        }
    }
}