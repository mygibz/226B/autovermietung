using System;
using System.Collections.Generic;
using Autovermietung.Exception;
using Newtonsoft.Json;

namespace Autovermietung
{
    [Serializable]
    public class Contract
    {
        // public Customer Customer { get; }
        // public Employee Employee { get; }
        // public List<Vehicle> Vehicles { get; }
        
        public bool Returned { get; set; }
        
        public long _validationDate; // saved as long (ticks), because of database (json)
        public long _expirationDate; // saved as long (ticks), because of database (json)
        public DateTime GetValidationDate() { return new DateTime(_validationDate); }
        public DateTime GetExpirationDate() { return new DateTime(_expirationDate); }
        public string[] Vehicles { get; set; }
        public float TotalCost { get; set; }
        public string CustomerName { get; set; }
        
        // private List<Equipment> _equipment = new List<Equipment>();

        public Contract(Customer customer, Employee employee, List<Vehicle> vehicles, DateTime validationDate, 
            DateTime expirationDate, float totalCost = -1, bool returned = false) {
            
            // calculate pricing of contract, if not overwritten
            if (totalCost < 1f)
            {
                float contractCost = 0;
                foreach (var v in vehicles)
                    contractCost += v.PricePerDay * (expirationDate - validationDate).Days;

                TotalCost = contractCost;
            }
            else
            {
                TotalCost = totalCost;
            }

            Returned = false;
            
            if (customer.License == null || !customer.License.IsValid)
            {
                throw new InvalidLicenseException(customer);
            }

            if (vehicles.Count == 0)
            {
                throw new EmptyContractException();
            }
            // Customer = customer;
            // Employee = employee;
            
            // Vehicles
            Vehicles = new string[vehicles.Count];
            for (var i = 0; i < vehicles.Count; i++)
                Vehicles[i] = vehicles[i].UUID;

            _validationDate = validationDate.Ticks;
            _expirationDate = expirationDate.Ticks;
            CustomerName = customer.Name;
            
            Returned = returned;
        }
        
        [JsonConstructor]
        public Contract(DateTime validationDate, DateTime expirationDate, float totalCost, bool returned = false)
        {
            _validationDate = validationDate.Ticks;
            _expirationDate = expirationDate.Ticks;
            TotalCost = totalCost;
            Returned = returned;
        }

        public bool IsValid()
        {
            DateTime currentDateTime = DateTime.Today;
            if (_validationDate < currentDateTime.Ticks)
            {
                return false;
            }
            if (_expirationDate > currentDateTime.Ticks)
            {
                return false;
            }
            return true;
        }

        // public List<Equipment> GetEquipment()
        // {
        //     return _equipment;
        // }
        //
        // public void AddEquipment(Equipment equipment)
        // {
        //     _equipment.Add(equipment);
        // }

        // public void PrintInfo()
        // {
        //     Console.Write($"Customer:\t{Customer.Name} ({Customer.Id})\n" +
        //                   $"Employee:\t{Employee.Name} ({Employee.Id}) - {Employee.Department}\n" +
        //                   $"Status: \t{(IsValid() ? "Valid" : "Invalid")}\n" +
        //                   $"Valid after:\t{ValidationDate}\n" +
        //                   $"Valid until:\t{ExpirationDate}\n");
        //     if (_equipment.Count == 0) return;
        //     Console.Write("Equipment:\n");
        //     foreach (Equipment equipment in GetEquipment())
        //     {
        //         Console.Write($"\t\t {equipment.Name} ({equipment.Description})\n");
        //     }
        //
        //     foreach (Vehicle vehicle in Vehicles)
        //     {
        //         vehicle.PrintInfo();
        //     }
        // }
    }
}