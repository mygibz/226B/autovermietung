using System;
using Newtonsoft.Json;

namespace Autovermietung
{
    [Serializable]
    public class Employee : Human
    {
        public WorkerType Department { get; set; }
        
        public string UUID => _uuid;
        private string _uuid { get; set; }

        public Employee(string name, Language nativeLanguage, WorkerType department = WorkerType.Other) : base(name,
            nativeLanguage)
        {
            Department = department;

            _uuid = Guid.NewGuid().ToString();
        }
        
        [JsonConstructor]
        public Employee(string name, Language nativeLanguage, WorkerType department, string uuid) : base(name,
            nativeLanguage)
        {
            Department = department;

            _uuid = uuid;
        }
        
        public override void PrintInfo()
        {
            Console.Write($"Employee:\t{Name}\n");
            base.PrintInfo();
            Console.Write($"\t\t Department: {Department}\n");
        }
    }
}