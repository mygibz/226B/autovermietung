using System;
using System.Collections.Generic;

namespace Autovermietung
{
    [Serializable]
    public abstract class Human
    {
        public Guid Id { get; }
        public string Name { get; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public Language NativeLanguage { get; set; }
        private List<Contract> _contracts = new List<Contract>();

        protected Human(string name, Language nativeLanguage)
        {
            Name = name;
            NativeLanguage = nativeLanguage;
            Id = Guid.NewGuid();
        }

        public List<Contract> GetContracts()
        {
            return _contracts;
        }

        public void AddContract(Contract contract)
        {
            _contracts.Add(contract);
        }
        public virtual void PrintInfo()
        {
            Console.Write($"\t\t NativeLanguage: {NativeLanguage}\n" +
                          $"\t\t Id: {Id}valid\n");
        }
    }
}