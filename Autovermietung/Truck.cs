﻿namespace Autovermietung
{
    public class Truck : Vehicle
    {
        public int MaxLoad { get; set; }
        public int Axis { get; }

        public Truck(Manufacturer manufacturer, string model, float pricePerDay, int doors, int xWheelDrive,
            int maxLoad, int axis, bool isSelfDriving = false, bool hasTrailerHitch = false, Plate plate = null) :
            base(manufacturer, model, pricePerDay, doors, xWheelDrive, isSelfDriving, hasTrailerHitch, plate)
        {
            MaxLoad = maxLoad;
            Axis = axis;
        }
    }
}