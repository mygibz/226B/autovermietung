using System;

namespace Autovermietung
{
    [Serializable]
    public class Manufacturer
    {
        public string Name { get; }

        public Manufacturer(string name)
        {
            Name = name;
        }
        public void PrintInfo()
        {
            Console.Write($"Manufacturer: {Name}\n");
        }
    }
}