﻿namespace Autovermietung
{
    public class Van : Vehicle
    {
        public bool HasSideDoor { get; }
        public int MaxWeight { get; }

        public Van (Manufacturer manufacturer, string model, float pricePerDay, int doors, int xWheelDrive, int maxWeight,
            bool isSelfDriving = false, bool hasTrailerHitch = false, Plate plate = null, bool hasSideDoor = true) :
            base (manufacturer, model, pricePerDay, doors, xWheelDrive, isSelfDriving, hasTrailerHitch, plate)
        {
            HasSideDoor = hasSideDoor;
            MaxWeight = maxWeight;
        }
    }
}